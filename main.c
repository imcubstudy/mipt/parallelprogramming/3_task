#include <mpi.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

#define _cmp(op, x, y)                                              \
    ({                                                              \
        typeof(x) __x = (x);                                        \
        typeof(y) __y = (y);                                        \
        __x op __y ? __x : __y;                                     \
    })
#define min(x, y) _cmp(<, x, y)
#define max(x, y) _cmp(>, x, y)

#define REQUIRE(cond, msg)                                          \
    do {                                                            \
        if(!(cond)) {                                               \
            fprintf(stderr, msg "\n");                              \
            fflush(stderr);                                         \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)


void distribute(const int size, const int length);
double collect(const int size);
void job();

int mpi_rank = -1; 
int mpi_size = -1; 

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int length = atoi(argv[1]);

    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    double result = NAN;
    double time = 0;
    if(0 == mpi_rank) {
        time -= MPI_Wtime();
        distribute(mpi_size, length);
        result = collect(mpi_size);
        time += MPI_Wtime();
        printf("[RES] %lf [TIME] %lf\n", result, time);
    } else {
        job();
    }

    MPI_Finalize();
    return 0;
}

void distribute(const int size, const int length)
{
    REQUIRE(length > size - 1, "Not enough elements to distribute");
    int chunk_size = length / (size - 1);
    int chunks[size * 2];
    for(int i = 1; i < size; ++i) {
        chunks[i * 2] = chunk_size * (i - 1) + 1;
        chunks[i * 2 + 1] = chunk_size * i + 1;
    }
    chunks[size * 2 - 1] = max(length + 1, chunk_size * (size - 1) + 1);

    int dummy[2];
    MPI_Scatter(chunks, 2, MPI_INT, dummy, 2, MPI_INT, 0, MPI_COMM_WORLD);
}

double collect(const int size)
{
    double results[size];
    double dummy;
    MPI_Gather(&dummy, 1, MPI_DOUBLE, results, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    double result = 0;
    for(int i = size - 1; i > 0; --i) {
        result += results[i];
    }
    return result;
}

void job()
{
    int chunk[2];
    MPI_Scatter(NULL, 2, MPI_INT, chunk, 2, MPI_INT, 0, MPI_COMM_WORLD);

    double common_coeff = 6. / pow(M_PI, 2);
    double result = 0;
    for(int i = chunk[1] - 1; i >= chunk[0]; --i) {
        result += 1 / ((double)i * i);
    }
    result *= common_coeff;
    MPI_Gather(&result, 1, MPI_DOUBLE, NULL, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
}

